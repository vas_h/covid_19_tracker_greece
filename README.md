# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Data collection and visualization of COVID-19 for Greece including cases and vaccination

### How do I get set up? ###

* Get an API token from https://www.data.gov.gr/token/
* Copy .env.template to .env
* Add API toekn in .env 
* Set up ${PROJECT_HOME} in .env
* Install requirements.txt
* Navigate to ${PROJECT_HOME}
* Execute python -m ingestion.run_ingestion
* Data will appear under the folders specified in .env. These are ignored and not commited

### Contribution guidelines ###

### Who do I talk to? ###

* vas.h@protonmail.com