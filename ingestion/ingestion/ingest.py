import pandas as pd
import requests
import os
from bs4 import BeautifulSoup
from datetime import datetime, timedelta


def get_vaccinations(day, token):
    
    url = 'https://data.gov.gr/api/v1/query/mdg_emvolio?date_from=2020-12-27&date_to='+str(day)
    authorization = 'Token ' + token
    headers = {'Authorization': authorization}
    response = requests.get(url, headers=headers)
    
    df = pd.DataFrame(response.json())
    df["referencedate"] = pd.to_datetime(df["referencedate"])
    
    path = os.path.join(os.environ["DATA_RAW"],'emvoliasmoi_'+day+'.csv')
    df.sort_values(by=['referencedate']).to_csv(path, index=None)


def get_cases_and_tests(day):
    
    #change the date format from YYYY-MM-DD to DD-MM-YYYY
    day = day[-2:] +'-' + day[5:7]  +'-' + day[:4]
    url = "http://www.odigostoupoliti.eu/koronoios-krousmata-simera-"+day+"-stin-ellada/"
    
    # Make a GET request to fetch the raw HTML content
    html_content = requests.get(url).text
    
    # Parse the html content
    soup = BeautifulSoup(html_content, "lxml")
    table = soup.find("table")
    
    #find the headings
    table_data = table.tbody.find_all("tr")
    headers = []
    for td in table_data[0].find_all("td"):
        # remove any newlines and extra spaces from left and right
        headers.append(td.text.replace('\n', ' ').strip())
    
    table_data = []
    
    for tr in table.tbody.find_all("tr")[1:]:
        t_row = {}
        for td, th in zip(tr.find_all("td"), headers):
            t_row[th] = td.text.replace('\n', '').strip()
        table_data.append(t_row)
    
    df = pd.DataFrame(table_data)
    path = os.path.join(os.environ["DATA_RAW"],'cases_and_tests_'+day+'.csv')
    df.to_csv(path, index=None)

def get_cases_by_periferia(day):

    #change the date format from YYYY-MM-DD to DD-MM-YYYY
    day = day[-2:] +'-' + day[5:7]  +'-' + day[:4]
    url = "http://www.odigostoupoliti.eu/koronoios-krousmata-simera-"+day+"-stin-ellada/"
    
    # Make a GET request to fetch the raw HTML content
    html_content = requests.get(url).text
    
    # Parse the html content
    soup = BeautifulSoup(html_content, "lxml")
    
    paras = [p.text for p in soup.find_all("p")]
    marker = "ΑΝΑΤΟΛΙΚΗΣ ΑΤΤΙΚΗΣ"
    para = [p for p in paras if marker in p][0]
    res_1 = [s.replace(u'\xa0', u' ') for s in para.split('\n')]
    res_1 = list(map(lambda s: s.split(),res_1))
    res_1 = list(map(lambda l: (' '.join(l[:-1]), int(l[len(l)-1])), res_1))

    #get the one paragraph that contains the rest of periferies (ridiculous)
    marker = "Αιτωλοακαρνανία"
    para = [p for p in paras if marker in p][0]
    res_2 = para.split('\n')
    res_2 = list(map(lambda s: (s.split()[0],s.split()[1]),res_2))
    
    marker = "Θεσσαλονίκη"
    para = [p for p in paras if marker in p][0]
    res_3 = para.split()
    res_3 = [(res_3[1],res_3[2])]

    res = res_1 + res_2 + res_3
    df = pd.DataFrame(res)
    df['date'] = day
    df.columns = ["periferia","covid_cases","date"]

    #normalize periferia names
    mapping = pd.read_csv(os.path.join(os.environ["CONF"],"periferies_mapping.csv"))
    mapping = dict(zip(list(mapping["from"]),list(mapping["to"])))
    df["periferia"] = df['periferia'].replace(mapping)
    path = os.path.join(os.environ["DATA_RAW"],'cases_by_periferia'+day+'.csv')
    df.to_csv(path, index=None)
    


def get_pop_by_periferia():
    url = "https://el.wikipedia.org/wiki/%CE%9A%CE%B1%CF%84%CE%AC%CE%BB%CE%BF%CE%B3%CE%BF%CF%82_%CE%A0%CE%B5%CF%81%CE%B9%CF%86%CE%B5%CF%81%CE%B5%CE%B9%CE%B1%CE%BA%CF%8E%CE%BD_%CE%95%CE%BD%CE%BF%CF%84%CE%AE%CF%84%CF%89%CE%BD_%CE%95%CE%BB%CE%BB%CE%AC%CE%B4%CE%B1%CF%82_%CE%B1%CE%BD%CE%AC_%CF%80%CE%BB%CE%B7%CE%B8%CF%85%CF%83%CE%BC%CF%8C"
    url = "https://el.wikipedia.org/wiki/%CE%9A%CE%B1%CF%84%CE%AC%CE%BB%CE%BF%CE%B3%CE%BF%CF%82_%CE%A0%CE%B5%CF%81%CE%B9%CF%86%CE%B5%CF%81%CE%B5%CE%B9%CE%B1%CE%BA%CF%8E%CE%BD_%CE%95%CE%BD%CE%BF%CF%84%CE%AE%CF%84%CF%89%CE%BD_%CE%95%CE%BB%CE%BB%CE%AC%CE%B4%CE%B1%CF%82_%CE%B1%CE%BD%CE%AC_%CF%80%CF%85%CE%BA%CE%BD%CF%8C%CF%84%CE%B7%CF%84%CE%B1_%CF%80%CE%BB%CE%B7%CE%B8%CF%85%CF%83%CE%BC%CE%BF%CF%8D"
    # Make a GET request to fetch the raw HTML content
    html_content = requests.get(url).text
    
    # Parse the html content
    soup = BeautifulSoup(html_content, "lxml")
    table = soup.find("table")
    
    t_headers = []
    for th in table.find_all("th"):
        # remove any newlines and extra spaces from left and right
        t_headers.append(th.text.replace('\n', ' ').strip())
    
    # Get all the rows of table
    table_data = []
    for tr in table.tbody.find_all("tr"): # find all tr's from table's tbody
        t_row = {}
        for td, th in zip(tr.find_all("td"), t_headers): 
            t_row[th] = td.text.replace('\n', '').strip()
        table_data.append(t_row)
    
    df = pd.DataFrame(table_data)
    path = os.path.join(os.environ["DATA_RAW"],'pop_by_periferia.csv')
    df.to_csv(path, index=None)

def make_data_dirs():

    if not os.path.exists(os.environ["DATA_RAW"]):
        print("making dir ", os.environ["DATA_RAW"])
        os.makedirs(os.environ["DATA_RAW"])
    if not os.path.exists(os.environ["DATA_INTERIM"]):
        print("making dir ", os.environ["DATA_INTERIM"])
        os.makedirs(os.environ["DATA_INTERIM"])
    if not os.path.exists(os.environ["DATA_PROCESSED"]):
        print("making dir ", os.environ["DATA_PROCESSED"])
        os.makedirs(os.environ["DATA_PROCESSED"])

def  compute():
    
    token = os.environ["TOKEN"]
    make_data_dirs()

    #get historical cases
    if eval(os.environ["GET_HISTORICAL_CASES"]):
        start_date = datetime.strptime(os.environ["START_DATE"],'%Y-%m-%d').date()
        end_date = datetime.today().date()  - timedelta(days=1)
        dates = pd.date_range(start_date,end_date,freq='d')
        for  date in dates:
            print("getting historical data for ", str(date.date()))
            get_cases_by_periferia(str(date.date()))


    #ingestion
    print(" getting population by periferia")
    get_pop_by_periferia()
    #get yesterdays' data
    if eval(os.environ["GET_DAILY_SNAPSHOTS"]):
        date = datetime.today().date() - timedelta(days=1)
        date = str(date)
        print(" getting daily cases and tests for ", date)
        get_cases_and_tests(date)
        print(" getting daily vaccinations for ", date)
        get_vaccinations(date, token)

