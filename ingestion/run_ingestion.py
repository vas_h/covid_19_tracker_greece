from dotenv import find_dotenv, load_dotenv
from ingestion.ingestion.ingest import compute
#from skills_pricer.skills_data_prep.skills_data_prep import compute

# Load .env in the launcher as this might be not required in deployed version
load_dotenv(find_dotenv())

def main():
    compute()

if __name__ == "__main__":
    main()
